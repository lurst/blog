MAX=$(ls -1 ./content/posts/*.md 2>/dev/null | sed -n "s|.*/\\([0-9]*\\)-.*|\\1|p" | sort -n | tail -n1)
MAX=${MAX:-0}
NEXT=$(printf "%04d" $((10#$MAX + 1)))
NAME=$(echo "$1" | sed "s/ /-/g")
touch "./content/posts/$NEXT-$NAME.md"

echo "$NEXT-$NAME.md"
