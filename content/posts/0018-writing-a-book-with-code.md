Title: Writing a book using code
Date: 2022-07-06
tags: generative, goals, nanogenmo
Summary: This is a write up on a talk with the same name, that I've given at my company back in 2018.

This is a write up on a talk with the same name, that I've given at the company
I was working at back in 2018.

---

## NaNoWriMo

Do you know what NaNoWriMo is? (Question to the audience)

It's the National Novel Writing Month, where some people commit to writing a book with
50,000 words during the month of November.

But I'm not a writer (yet), so this doesn't really interest me much, I write code, not prose
(usually). I'm not alone in this, so someone created the NaNoGenMo.

## NaNoGenMo

Do you know what the NaNoGenMo is? (Question to the audience)

The NaNoGenMo stands for National Novel Generational Month, which has the same goal of
writing a book with 50,000 words during the month of November, _but_, you're writing code
that will write it, instead of writing it yourself.

## Why?

I've always been interested in generative programming, so this would be a great excuse to
"be" around people doing the same thing, with a very specific goal and deadline. I figured
this would motivate me to actually start *and* finish this project.

## My Idea

My idea for the book was to have a sort of dungeon crawler log book, where adventurers would
go on a dungeon in pursuit of a treasure in the end of it, having to fight monsters in the
way, get better gears and maybe find traps too.

I wanted to simplify the idea because I didn't want to generate floors and rooms for the
dungeon and having to have adventurers decide which way to go through every time. So my
solution to simplify it was to have a single corridor where there was just one path to take,
I'd shuffle enemies and treasures, and it would make it interesting enough (hopefully).

So, with that idea and a whole month to execute, I spent hours after work playing with ideas
on how to generate weapons, generate enemies, creating my own super simple combat system,
creating different versions of the book, discovering new ideas from mistakes I made, getting
tired of my own repetitive words and making it all better.

## Learnings

Code-wise, it didn't look good, I did not use best practices like, TDD, I was at times
coding blindly, refactoring whole chunks without running the code a single time, it wasn't
pretty. I did learn some things, I managed to improve a library I built that was perfect for
this kind of project ([dragn](https://github.com/lurst/dragn)), I learned about `pandoc`,
`epubs` formatting, it was great! I also would check out other people's projects as a way to
"productively procastinate" which gave me more ideas and a whole bunch of motivation.

Overall, a good time!

But, I actually lied, this talk is not about how to write a book with code, this is actually
about learning something and doing something I've wanted to do with a specific goal and
deadline.

So I ask you, when are you going to set a deadline, a specific goal so you can find the time
to finally do that thing you want to for a while?
