Title: My current analogue systems
Date: 2025-01-10
tags: journaling, everbook, bullet journal, analogue productivity
Summary: What am I using right now to organize myself and remember things

It's been a minute since my last post, but today feels like the perfect time to
jump back in, and on a subject I haven't talked much about in this blog, my interest
for analogue systems, using pen and paper as a way to take notes, capture ideas
and reflect on them.

For context, I've been using the bullet journal system on and off since 2013 when I saw
it first posted on hacker news, before that, from 2008, I had a moleskine notebook I
adored that I would fill up with everything and anything, doodles, tech stuff, todo
lists, I still have a trip to the UK that I captured using drawings that I know very
little about, other than the supermarkets I went to since that's the only thing I noted
on those drawings.

The bullet journal system helped me with giving some structure to my notes, so that I
could not only take notes quicker using the rapid logging, but also be able to recall
them better, using the index and collections. I've been using it successfully from that
point on, I switched to digital notes from time to time, either with org-mode or
vimwiki or just a file on my computer called `todo.txt`.

Roughly since 2022 I have been back in almost full analogue mode (I say almost because I
still keep some big todo lists and projects under org-mode, it's hard to copy paste from
there into my notebooks) simply because I missed it and I think I end up asking myself
to do fewer things (smaller todo lists).

When I say I was in full analogue mode, I mean that I've kept one notebook with me
(mostly a Leuchtturm bullet journal 2nd edition) which has lasted me roughly 1 year,
when I feel like the notebook is almost full, I switch, leaving some paper behind,
simply because I like to keep 1 full month on the notebook rather than splitting them
since I rely on the monthly view for my habit tracker.

While the bullet journal system was working mostly fine for me, I disliked how
collections (a collection is a page about anything that isn't the day-to-day stuff)
could become lost in the middle of my notebook, or worse, if it was good, having to
refer to an old notebook for something that was too big to transfer into the new one.
Which led me to research a system I've known for a while...

Enter [Everbook](https://everbookforever.com/), a system for keeping various notebooks
by using only sheets of paper and folded sheets of paper to hold them together,
which allows for unlimited "notebooks" and unlimited pages in the "notebooks". This
is also a reason why I enjoyed keeping a reMarkable tablet, but I'll write my
experiences with it in another post.

Starting to try out the everbook system, I was very happy to see that there's a
[new channel](https://www.youtube.com/@I_wrote_it_down) in YouTube picking up the
slack since the [official YouTube channel](https://www.youtube.com/@everbook425)
and [reddit community](https://www.reddit.com/r/everbook/) have been silent for
the last 2 years, I may write more posts specifically about this system since
there's very little resources about it.

The reason why I was very keen to try this system out is as a way to commonplace.
Commonplace notebooks and I'm being very general with my own definition of it, is a
notebook that contains a bunch of different information about many different things. If
you want a more academic and specific definition, check Parker Notes YouTube channel.

For me, keeping a commonplace has been a struggle because I don't like dedicating an
entire notebook to a subject I don't know if I will be interested in more than a week,
even a small fieldnotes that has 48 pages feels too big for something I don't know
whether I will actually write more than 10 pages about.

With everbook, I can start by writing one page, another page, fold one sheet and keep
them both inside, if I write more, great, I can add more sheet, if I don't, then I can
simply store that folder away for later, no notebook has been unused.

Another good use is for work, I was keeping some cheap notebooks for writing things
for work because I didn't want to mix work with my own personal things, so now I can
keep a few sheets of paper dedicated to work and throw them away when I don't need them,
or have already moved the information to work's Notion or Jira.

So far I've been enjoying the system and I hope to write more over here about this, do
you also keep commonplace books? How do you organize information about subjects you may
not be interested in a few days? Have you found any hybrid systems that combine analogue
and digital tools effectively (been looking for something like this for a while)?

Bye for now
