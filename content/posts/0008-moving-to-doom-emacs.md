Title: Moving to Doom Emacs
Date: 2020-07-03
Tags: emacs, tools
Modified: 2020-07-05
Summary: Why I moved from neovim to doom emacs as my main code editor

It's funny how my previous post was about my favourite vim plugins when I have recently switched to doom emacs as my main code editor.

## Why did you move to emacs?

Ever since I started using a lot of plugins with vim, I've been thinking whether I really should be using vim. Even though my vim is still quite fast to load (252ms checked using `nvim startuptime`), I still felt like I was bloating it too much, and that I should try to reduce them or use something that's built for being extensible.

This doesn't really make much sense, I admit. For the record, I think it's OK for anyone to use a vim configuration that takes a minute to load because it's bloated with plugins if in the end of the day, the person using it is happy and productive with it. But I just wanted to know how it felt to use an editor that was not only prepared for being extended, but that had extensibility as one of the main features.

Another thing that made me want to learn and use emacs was org-mode, I've been reading a lot about writing better notes and knowledge management systems, and org-mode looked like a great candidate for me to move from vimwiki.

## What about Spacemacs?

Some time ago, I saw [spacemacs](https://www.spacemacs.org/), and I tried it, I thought it was a cool idea, I really liked the vim emulation by evil-mode and _loved_ the idea of [which-key](https://github.com/justbur/emacs-which-key), which is the engine behind hitting spacebar and getting all the shortcuts and commands available, which makes learning spacemacs incredibly easy and fast. I liked it so much that I later installed the [same plugin](https://github.com/liuchengxu/vim-which-key) for vim. But in the end, I found it too bloated, there's a whole layer system built on top of emacs (therefore more things to learn other than emacs), so I just uninstalled emacs and never came back.

## Enter Doom emacs...

More recently I found out about [doom emacs](https://github.com/hlissner/doom-emacs), a very similar emacs config framework with a lot of the same things from Spacemacs, except that it was much more lightweight and I found the `init.el` file (the one where you configure the modules you want to install) awesome because it comes with a lot of things turned off by default which I found a very tasteful move by the author (instead of a community, doom emacs has a main developer, making it more opinionated), I really liked the default theme too.

Setting out to try it, I first started by using it on my personal laptop to code my little pet projects, and after coding almost a whole serverless script from Doom Emacs, I felt ready.

I used it during work, it was painful early on, but the community at their discord channel was incredibly helpful, and quick to reply, what surprised me the most was that the author regularly goes around the noobies channels answering every unanswered question.

## Conclusion

I am now confortably writing this blog post on emacs, and while I still miss the terminal, and I haven't managed to seamlessly use the terminal on emacs like I had it setup with tmux + vim, I am diving deeper and deeper into the emacs world.

Maybe my next post will be about emacs, who knows ;).
