Title: Dealing with packages not owned by `pacman`
Date: 2020-02-09
Tags: arch linux,pacman
Modified: 2020-07-05
Summary: How to know where the files associated with a package are

I had installed python 3.5 using `yay` (an installer for packages from Arch's [AUR packages](https://aur.archlinux.org/))
since it didn't exist in the pacman repos, after removing that package, I started having issues with `pip` because it was
bound to the `python35` package which no longer existed.

I looked up how to figure out which package owned the pip file and found this command:

```sh
$ sudo pacman -Qo /usr/bin/pip
error: No package owns /usr/bin/pip
```

That's good, that means I just need to remove the file.

```sh
$ sudo rm /usr/bin/pip3
```

And then install the proper `python-pip` package...

```sh
$ sudo pacman -S python-pip
error: failed to commit transaction (conflicting files)
python-pip: /usr/bin/pip3 exists in filesystem
Errors occurred, no packages were upgraded.
```

But that didn't work because the file `pip3` (from the old `python35` package) was still installed. I just repeated the
same thing I did to `python35` to check that it was not owned by any `pacman` package, and fortunately, that was the only
last file I needed to purge.

Now I was left with a clean system, and I can install `python-pip` normally with:

```sh
$ sudo pacman -S python-pip
```
