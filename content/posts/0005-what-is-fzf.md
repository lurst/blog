Title: What is fzf
Date: 2020-02-23
tags: tools, command-line
Summary: FZF can make your command line usage a lot more fun

I use the terminal a LOT, my main editor is [neovim](https://neovim.io/), and all external commands are run from a terminal in a [tmux](https://github.com/tmux/tmux/wiki) session usually. The only GUI I use is a browser, everything else, I give preference to [TUI](https://en.wikipedia.org/wiki/Text-based_user_interface)s.

Being quick is part of the advantage of the terminal, I can touch type, command have usually a quick output which shortens the feedback loop of whatever I am doing and that makes me incredibly happy. Also, I've taken a lot of time to learn how to use the shell effectively, so it's easy to solve issues like weird output with a dash of `awk` or `sed` when needed. Call this [Stockholm syndrome](https://en.wikipedia.org/wiki/Stockholm_syndrome) if you want, I don't care.

I think the first time I used fzf was together with vim as a replacement for [Ctrl+P](https://github.com/kien/ctrlp.vim), but since I was used to it at the time, I didn't really switch, I liked the idea, but didn't really push it much. I started using this [tool](https://github.com/pmazurek/aws-fuzzy-finder) at work, which made me a lot more productive (I hate logging in to AWS console) but again, I only used that tool with `fzf` and that was it.

Recently, I tried the [`fish`](https://fishshell.com/) shell and liked it a lot, so I switched permanently from `zsh` with `oh-my-zsh` without plugins. But, I had some issues, one of them was my use of `virtualenv-wrapper` which is a tool for managing python virtual environment.

Using `virtualenv-wrapper` and an alias I had, I could `cd` into a project and type `work` to activate the project's python environment. This stopped working for various reasons in `fish`. I [found an alternative](https://github.com/LuRsT/Setup/blob/2b19df3ddc9a779c6c5cbbb0761be80fd28d125a/.config/fish/functions/workon.fish) using `fzf` in `fish`. And I was delighted by it, I'm not even bothered that it takes me more key presses than before, because I'm not using external libraries to manage my virtual environments anymore.

---

# What exactly is `fzf`?

From the [project page](https://github.com/junegunn/fzf):

_It's an interactive Unix filter for command-line that can be used with any list; files, command history, processes, hostnames, bookmarks, git commits, etc._

Still confused? Here's some examples:

Let's say I have a list of things, and I'd like to pick one for some reason.

```sh
$ echo -e "Bulbasaur\nCharmander\nSquirtle"
Bulbasaur
Charmander
Squirtle
```

That `echo` gives us a list of items, if we pipe them into `fzf`, we now have a selector AND a fuzzy searcher.

```sh
$ echo -e "Bulbasaur\nCharmander\nSquirtle" | fzf
```

Result:

![pokemon_selector](images/pokemon-select-fzf.gif)

That's the very basic functionality of `fzf`, you pass it a list of things, you get the `fzf` selector and when you select the item, it outputs it.

If you haven't figured out yet what to do with it, stay tuned for future posts about the marvelous things you can do with `fzf`.
