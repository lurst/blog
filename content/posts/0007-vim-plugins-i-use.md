Title: Vim plugins I use
Date: 2020-04-13
Tags: vim, plugins
Modified: 2020-07-05
Summary: All the vim plugins I have at this time

Here are the vim plugins I use right now.

## Plugin [sainnhe/gruvbox-material](https://github.com/sainnhe/gruvbox-material)

This is my current colorscheme, I'm enjoying it so far, I use it with the "medium" background.

## Plugin [airblade/vim-gitgutter](https://github.com/airblade/vim-gitgutter)

This plugin displays a character on the gutter (the left of the line number) with either a `+`, a `-` or `~` meaning the changes against the git stage. Very useful to see what changes I made and whether I've made too many changes and should commit.

## Plugin [alok/notational-fzf-vim](https://github.com/alok/notational-fzf-vim)

Notational Velocity is a [whole system](http://notational.net/) where you can easily search notes and create new ones from a simple input. This plugin gives vim that functionality. I haven't used it much, but it looks good.

## Plugin [bogado/file-line](https://github.com/bogado/file-line)

This plugin allows you to open files with the format `file.txt:34` where `34` is a line number. With it, running `vim file.txt:34` opens vim with that file on that line number.

## Plugin [cespare/vim-toml](https://github.com/cespare/vim-toml)

Syntax highlighting for [toml](https://github.com/toml-lang/toml) files.

## Plugin [chase/vim-ansible-yaml](https://github.com/chase/vim-ansible-yaml)

Syntax highlighting for Ansible playbooks.

## Plugin [christoomey/vim-tmux-navigator](https://github.com/christoomey/vim-tmux-navigator)

Some very useful key combos for moving from vim to tmux panes with a simple Control+hjkl.

## Plugin [dense-analysis/ale](https://github.com/dense-analysis/ale)

Ale is an engine for linting files asynchronously, it does a LOT more than that, but I haven't explored it enough, right now, it just works.

## Plugin [easymotion/vim-easymotion](https://github.com/easymotion/vim-easymotion)

Pressing `s` (not sure if you need to configure it or not) in normal mode asks me for a letter and gives a name (just a letter or two) to every character in the screen with that character. Easier to watch than explain, this allows you to drive vim very quickly and intuitively.

## Plugin [editorconfig/editorconfig-vim](https://github.com/editorconfig/editorconfig-vim)

Enables [Editorconfig](https://editorconfig.org/) support in vim.

## Plugin [gabesoft/vim-ags](https://github.com/gabesoft/vim-ags)

A great way of searching for strings in your projects, it shows in a new window with a bit of context + it allows for multiple editing. I can probably replace it with fzf or something else, but I really like the other features. You don't have to use `ag` for searching.

## Plugin [hotwatermorning/auto-git-diff](https://github.com/hotwatermorning/auto-git-diff)

A plugin that shows the diff during a `git rebase -i`, incredibly helpful to see what the commit contains, especially when you simply write "Fixup" or "WIP" in the commit message.

## Plugin [itchyny/lightline.vim](https://github.com/itchyny/lightline.vim)

The plugin that drives my statusbar, light and configurable.

## Plugin [junegunn/fzf](https://github.com/junegunn/fzf)

Along with [junegunn/fzf.vim](https://github.com/junegunn/fzf.vim) make my vim amazing, it comes with fuzzy search for many things, most notably `:Files`, I replaced `Ctrl+P` with this plugin, it even allows for a pop-up now!

## Plugin [junegunn/goyo.vim](https://github.com/junegunn/goyo.vim)

Distraction free writing in vim.

## Plugin [junegunn/vim-peekaboo](https://github.com/junegunn/vim-peekaboo)

Shows the registers in the right side, so it's easy to see what things are in which register. Also shows you the macros.

## Plugin [lifepillar/vim-colortemplate](https://github.com/lifepillar/vim-colortemplate)

A plugin to help develop vim colorschemes using colortemplates.

## Plugin [lilydjwg/colorizer](https://github.com/lilydjwg/colorizer)

Display the color of a hex or rgb as the background. Very useful for writing CSS.

## Plugin [liuchengxu/vim-which-key](https://github.com/liuchengxu/vim-which-key)

Shows what each shortcut after pressing `Leader` does. I loved this feature in Spacemacs and when I saw this plugin, I just had to add it. Been using `Leader` a lot more now.

## Plugin [machakann/vim-highlightedyank](https://github.com/machakann/vim-highlightedyank)

Shows a small flash when you yank something. Not super fancy, but I love seeing it.

## Plugin [majutsushi/tagbar](https://github.com/majutsushi/tagbar)

Shows a tag bar on the right with every function/variable etc... Helpful when reading big files.

## Plugin [mxw/vim-jsx](https://github.com/mxw/vim-jsx)

Syntax highlighting for `jsx` files.

## Plugin [plasticboy/vim-markdown](https://github.com/plasticboy/vim-markdown)

Syntax highlighting for `markdown` files.

## Plugin [rust-lang/rust.vim](https://github.com/rust-lang/rust.vim)

Tools to help developing with Rust.

## Plugin [scrooloose/nerdtree](https://github.com/scrooloose/nerdtree)

A file navigator on the left of your files, just like on other editors. I use it with [ryanoasis/vim-devicons](https://github.com/ryanoasis/vim-devicons) so it looks prettier.

## Plugin [shougo/deoplete.nvim](https://github.com/shougo/deoplete.nvim)

Autocomplete engine that makes coding a lot more pleasant, I use it with [zchee/deoplete-jedi](https://github.com/zchee/deoplete-jedi) for python.

## Plugin [tpope/vim-commentary](https://github.com/tpope/vim-commentary)

Helps comment out lines of code in different languages.

## Plugin [tpope/vim-eunuch](https://github.com/tpope/vim-eunuch)

Adds a few file utils withing vim, such as removing a file you're editing.

## Plugin [tpope/vim-fugitive](https://github.com/tpope/vim-fugitive)

An amazing Git visualiser within vim.

## Plugin [vimwiki/vimwiki](https://github.com/vimwiki/vimwiki)

A wiki engine for Vim.
