Title: Servant Leadership vs Transformational Leadership
Date: 2020-02-20
Tags: leadership
Modified: 2020-07-05
Summary: Why you should lean towards transformational leadership instead of servant leadership

I read the term _Transformational Leadership_ in the book "The Project Unicorn", followed by the following description: "For the leader, it no longer means directing and controlling, but guiding, enabling and removing obstacles (...) It requires understanding the vision of the organization, the intellectual stimulation to question the basic assumptions of how work is performed, inspirational communication, personal recognition, and supportive leadership (...) Some think it's about leaders being nice (...) nonsense. It's about excellence, the ruthless pursuit of perfection, the urgency to achieve the mission, a constant dissatisfaction with the status quo, and a zeal for helping those the organization serves.".

_Pause for effect._

That was the best part of the book so far (I haven't finished it yet), it made me think about my own style of leadership and what am I doing to _transform_ my team into something better, greater.

It is also all that I know at this moment about the term, there is probably a lot more written about it which I will research after writing this post.

These are just my thoughts on the term and how it relates to _Servant Leadership_ in my opinion, at this moment in time.

My interpretation of the _Servant Leadership_ management style is: Work on improving the environment for the people I support by removing obstacles for them, help them solve their problems and do the thankless work that's needed so they can be happy at their job, focus more on what they are best at, in order to have a high performing team of unburdened Engineers.

I realise even just writing the above and knowing what I know now, that it's a very simplistic view, it dis empowers Engineers, takes away opportunities for them to grow, and underestimates them by treating them like one trick ponies that can only write code.

This could very well be a misinterpretation of the term on my part.

What I've been doing, isn't very much like what I described though, while I don't micromanage nor dictate what people should do, what I do is: I empower my team not by doing tasks instead of them, but instead, delegating them as opportunities for them to grow, give them guidance on how they should do things instead of dictating, and look for opportunities for the team to improve. And this is much closer to the definition above of _Transformational Leadership_.

What I learned from that passage is not to look at myself as a servant to the team, but look at myself as a catalyst of change, as someone who pushes others to be better, to do better.

Instead of removing obstacles, to show them to my team and ask: "How are we going to fix this?"
