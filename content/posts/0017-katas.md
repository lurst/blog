Title: Katas
Date: 2022-07-05
tags:
Summary: Katas, what are they and which ones I enjoyed doing, especifically refactoring Katas

I like doing katas, particularly refactoring katas. I've been creating some
small repositories for refactoring katas in Python3.

Refactoring katas are [katas](https://en.wikipedia.org/wiki/Kata_(programming))
where you're given some code that works (sometimes) and the goal is to make it
readable, and sometimes, to add some features.

So one way to do them is by creating a golden test and/or creating unit tests
where you can isolate features and then refactor away once you're confident
you're not changing their behaviour.

So, here is the list of the katas I've gotten so far, I'm putting my own
solutions in branches so I don't pollute master with them.

Note: My solutions are not necessarily the best!

- [Trivia](https://github.com/lurst/trivia_kata)
- [Gilded Rose](https://github.com/lurst/gilded_rose_kata)
- [Robot Kata](https://github.com/lurst/robot_kata) (not a refactoring kata)
