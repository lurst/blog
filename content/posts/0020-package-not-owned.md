Title: Packages not owned by pacman
Date: 2022-07-11
tags: linux, arch linux, pacman, packages
Summary: How to deal with packages not owned py pacman, the Arch linux package manager

# Packages not owned by pacman (Arch linux package manager)

I had in the past installed python35 using `yay`, after removing that package, I started having issues using
`pip` because it was bound to the python35 package which no longer existed.

I looked up how to figure out which package owned the pip file and found this command:

```
$ sudo pacman -Qo /usr/bin/pip
error: No package owns /usr/bin/pip
```

So, the easy choice was to simply remove the file.

```
sudo rm /usr/bin/pip3
```

And then install the proper `python-pip` package...

```
sudo pacman -S python-pip
...
error: failed to commit transaction (conflicting files)
python-pip: /usr/bin/pip3 exists in filesystem
Errors occurred, no packages were upgraded.
```

...which didn't work because there still was `pip3` here, so I repeated the operation above: checked that there was no package owning that file and removed the file.

Re-tried installing `python-pip` and it worked.
