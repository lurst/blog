Title: My take on the article: Theory-building...
Date: 2023-03-26
Modified: 2023-03-27
tags: software development, knowledge transfer, team management, extreme programming, best practices
Summary: You should try out ripgrep, a small introduction on why it's superior to grep and friends.

# My take on the article: Theory-building and why employee churn is lethal to software companies

Original article: [https://www.baldurbjarnason.com/2022/theory-building/](https://www.baldurbjarnason.com/2022/theory-building/)

## First, a little summary of my understanding of the article

Baldur is suggesting in this article building software is theory building, and since the engineers are the ones holding the mental models in their heads, replacing a team of engineers too quickly can cause the software to end up in an endless cycle of rewrites because every new engineer will lack the mental models, and almost forcing them to rewrite the existing code so they can make it understandable by them, the conclusion to this is that “churn is destructive” to the project.

Baldur proposes then that changing processes does nothing to mitigate this, that "skill" is what it takes (whatever that means...), that the team management has to be addressed by "a manager who understands the fundamental nature of software development" (hand waves), and the only way to address this is by having all second generation engineers (the new engineers joining a team) work with the first generation engineers (the ones who were already working in the project) so that they can transfer their mental models.

## Do I agree with it?

Not entirely, software building isn’t “just” theory building, we have code to look at, that we can read and understand (hopefully), first generation engineers are useful for getting a better understanding of the code and the history, which can be helpful for future decisions and not rewriting code that will have bugs that the legacy one solved already.

I think that we have lots of tools at our disposal to “put down these mental models” apart from the code written and the software built:
- We have documentation
- We have the working software
- We have other people apart from engineers who were a part of the project
- The use of frameworks in the code
- Software engineering practices such as pair programming and testing

### Documentation

Documentation is a great way of transferring knowledge and mental models, and we can have many different ways of documenting our code:
- Comments in the code for when you need to explain non-clear parts of the code
- Git commits for when you want to explain _why_ you made that change
- Architecture decision records for the higher level explanation of bigger decisions surrounding the software
- Wikis/User guides for holding it all together and sharing it with people outside of the team.
- Diagrams and flow charts for better explaining architecture or algorithms.

### Working Software

The actual working software isn’t just the outcome of the project, but a current manifestation of the “theory” the engineers are building and is a good way to understand what the code is doing.

### Other people

Engineers don’t work in a vacuum and it’s sad that this was not mentioned in the article, but project managers understand the project and have something to contribute, product managers most times know more about the product than the engineers themselves, QA understand the product in a way not even engineers will understand, the users can be interviewed, UX and Designers have a whole different perspective on the project but also knowledge that is part of this “theory” the engineers are building, etc…

### Frameworks

Frameworks, especially opinionated ones can help by just having an agreed way of doing things, there are engineers/projects where the frameworks are bent into submission, but you still have some core that new engineers can look at and understand and helps with transferring the mental models.

### Software engineering practices

A lot of the Extreme programming practices can help and are defined exactly with the purpose of sharing understanding about the project, such as pair programming and testing.

The conclusion of the author is that processes can’t help you get out of this, but extreme programming practices (arguably a bunch of processes) absolutely debunks that conclusion.

## Finally

The article has some truth, but it’s very one-sided and has a few blind spots that I think we as engineers should talk about more.
I think what's more important than worrying about theory building, is to create and foster a culture of learning and collaboration within the team, this will help in both the information sharing between people, but will also create a more psychology safe team, which in turn creates a more resilient team.
