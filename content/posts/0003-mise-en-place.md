Title: Mise en place
Date: 2020-02-11
Tags: productivity
Modified: 2020-07-05
Summary: Keeping a workpace clean before starting to work has a big impact on my productivity

_"Mise en place"_ is a term I learned the other day on a Hacker news [thread](https://news.ycombinator.com/item?id=22235279),
it describes ["the setup required before cooking"](https://en.wikipedia.org/wiki/Mise_en_place). Apparently, cooks usually prepare
everything they need before cooking a meal, this includes having a clean kitchen, putting all the ingredients in already measured
containers, so that the process of cooking is simply adding the ingredients, stirring and waiting, which gives them time to prep a
bit more as they cook.

This relates to the 3rd law described in the book [Atomic Habits](https://jamesclear.com/atomic-habits), for making a habit
easier, the point (3.2 from the [cheatsheet](https://atomichabits.com/cheatsheet)) "Prime the environment. Prepare your
environment to make future actions easier.", it's also similar to the aesthetic pleasing
[Knolling](https://en.wikipedia.org/wiki/Tom_Sachs_(artist)#Knolling), which are two things that I very much like, but don't
actually use on my day to day.

Today, as I was working from home, I decided to use it on two opportunities, for cooking and for working.

For cooking, I simply tried to imitate what I watched in cooking shows, I was making a stew (I've been making lots of them lately)
so, I took the meat out, the board, the knife, chopped it up in blocks, set it aside inside a bowl, peppered it up. I then, turned
the hob on, dumped the bacon lardons on started stirring, and once they were looking good, I dumped the ready beef blocks and turned
until they were brown, I dumped the tomato sauce and covered it. Now I could clean up the board, knife and bowl and get the second
part ready...

As for working, I have a confession to make: I have a tab addiction, I open tabs that takes me weeks to close, I leave them there
as reminders to read/watch/whatever with them, and then I can finally close them. I've seen worse, but I definitely could definitely
could use _"mise en place"_ to help me focus more on the task at hand.

Here's what I did: I instapapred, put on a .txt file all the URLs that I could, I did any task that could be done in 5min, such as
replying to emails, making a small change to a google doc, etc... Once I had almost 0 tabs on my browser, I could finally open the
tab I needed for the task at hand, and do it without the clutter of all the other unrelated tabs trying to get my attention.

I also cleaned up a bit my desk, but I wasn't as successful at that, moving on...

This post is both a reminder to myself that it felt _amazing_ to work this way, and that I should do this more often. I think that
if I manage to keep this up as a habit, I can be a lot happier.

