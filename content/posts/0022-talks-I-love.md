Title: Talks I love
Date: 2023-06-25
tags: software development, talks
Summary: A list of talks I like to watch and recommend other developers to watch

I love many talks, unfortunately, I am very forgetful, this is a way for me to keep them tracked so I can rewatch and recommend to other people great talks.

I'll keep this list with only talks related directly to software engineering.

- [Culture and Revolution by Ben Horowitz](https://a16z.com/2017/03/04/culture-and-revolution-ben-horowitz-toussaint-louverture/)
- [Container fun and hacks by Jessie Frazelle](https://www.youtube.com/watch?v=cYsVvV1aVss)
- [Principles of technology leadership by Bryan Cantrill](https://www.youtube.com/watch?v=9QMGAtxUlAc)
- [Wat by Gary Bernhardt](https://www.destroyallsoftware.com/talks/wat)
- [Beyond PEP8 By Raymond Hettinger](https://www.youtube.com/watch?v=wf-BqAjZb8M)
- [Inventing on Principle by Bret Victor](https://www.youtube.com/watch?v=PUv66718DII)
- [Get Off the Tightrope by Tom Stuart](https://www.youtube.com/watch?v=TdBELZG0UMY)
- [The Clean Architecture in Python by Brandon Rhodes](https://www.youtube.com/watch?v=DJtef410XaM)
- [7 minutes, 26 seconds, and the Fundamental Theorem of Agile Software Development](https://www.youtube.com/watch?v=WSes_PexXcA)

I may create a page for this, for now, this is just a post.
