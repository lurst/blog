Title: WriteTheDocs DIY conference
Date: 2020-10-17
tags: documentation, conferences
Summary: I forgot to buy a ticket for writethedocs, so I decided to make my own...

Tomorrow the [Writethedocs Prague conference](https://www.writethedocs.org/conf/prague/2020/) starts, I wanted to partake, but I unfortunately forgot to buy a ticket for it :(. Because it's a conference about writing documentation from a community with the same goal, of course they have some documentation about the conference itself, so why not build my own conference from previous talks?

I'm about to have some time off next week before starting a new job, so I have plenty of time to spare to both build the conference and participate in it. There are some pros/cons about doing this by myself, let's map them:

### Pros

- It's free! It only costs time, which I have.
- I can pick only the talks that I like (I still have the same issue as in a real conference because I can't know how good a talk is before actually watching it)
- I can speed the talks up, rewind, or skip entirely if I'm not enjoying it, I could do most of this in a usual remote conference, but skipping a talk and picking another one to watch immediatly is a big advantage of this format.
- More focus, since there aren't people watching the conference with me, I can focus on watching the talk and taking notes.
- Total control of my time, the breaks are as long and as short as I want them to be, because I control the schedule.

### Cons

- I'm not supporting the conference organizers, this is the counter to the fact that it's free.
- The lack of community, since I'm watching the talks by myself, I lose the whole social benefits of having peers to discuss what's happening, which could lead to more content for me to check out later.

## Preparation

The first step is to gather a few talks that I'd like to watch, so I can build a rough schedule for the talk day. And for that, writethedocs has a [page](https://www.writethedocs.org/topics/#writing-words) with learning resources that I can read/watch, it's also thankfully curated by category so I can pick talks/articles from the categories that I'm interested in.

An event that happens at writethedocs is also the "Writing Day", which is like the "sprints" of a software conference where people contribute to open source projects, but for documentation, there's a few resources available to help me replicate it if I want to:
- [Writing Day](https://www.writethedocs.org/conf/portland/2019/writing-day/), the page that has all the information about this event.
- [List of projects to help](https://docs.google.com/spreadsheets/d/1HPAXim5gkPmwhANJwngWcg1d09KHqOj3vILn5Tc5V9A/edit#gid=2040962915)

## Actually doing it

Then I just need to follow my schedule with some discipline. This is just a write up on how I'm planning to do it, once I actually do it, I'll write a post on which talks/articles I watched/read, whether I did the "Writing Day" and how it was.

Here are some extra resources that I found useful from the WriteTheDocs website:

- [Recommended Books](https://www.writethedocs.org/books/)
- [WriteTheDocs Guide](https://www.writethedocs.org/guide/)

## Gratitude

I wouldn't be able to do any of this if the conference organizers didn't share the content from the conferences, and so that I thank all conference organizers for having such a clear and easy to follow website and for sharing the talks and articles from the conferences.
