Title: You should try ripgrep
Date: 2022-07-07
tags: cli, tools, grep, ripgrep, ack, silver-surfer
Summary: You should try out ripgrep, a small introduction on why it's superior to grep and friends.

# You should try ripgrep

Rip grep (`rg`) is the last searcher from the whole contest of "which is the fastest
searcher in the terminal?". `grep` > `Ack` > `ag` (the_silver_surfer) > `rg` from slowest to
fastest.

Rip Grep is written in Rust, but what's so good about it is that is respects the `.gitignore`
and automatically skips hidden files/directories and binary files, making it have to search
for fewer files, which is why it's faster than alternatives.

It uses [Rust’s regex engine](https://docs.rs/regex/*/regex/#syntax) by default,
alternatively you can use the flag `--pcre2` to use PCRE2 regex engine.

It has sensible defaults when you run it in a terminal, making it very similar to `ag` in
that regard, here is an example of the results of a search:

```sh
$ rg def
main.py
13:def new_lorry():
20:def Truck(truckNo):
37:def maybe_add():
44:def update():
65:def draw():

app/queue.py
6:    def __init__(self, size, delay_range=None, feeder_queue=None):
14:    def length(self):
17:    def update_delay_range(self, delay_range):
22:    def handle_new(self):
27:    def add(self, item):
37:    def pop(self):
42:    def check_complete(self):
```

You can't see the colors, but they are enabled as default. Along with line numbers and the
file path.

You can (and should) integrate it with vim with the `--vimgrep` flag, with `fzf` by using it
as the default searcher.

Another very useful flag is the `--hidden` which makes `rg` also search inside hidden files
and folders.
