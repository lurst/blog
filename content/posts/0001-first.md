Title: Back to a normal blog site
Date: 2020-02-08
Tags: meta
Modified: 2020-07-05
Summary: New static site generator, new site...

After trying out GitBook for my site and trying to create a kind of wiki for myself, I'm going back to a normal blog, since I realised that having a wiki, made me less keen to write often, having a date on my posts, may help me see how consistent I'm being with my writing.

I'm using Gitlab's CI to publish the blog, copied their [Pelican examples](https://gitlab.com/pages/pelican), and picked this [theme](https://github.com/laughk/pelican-hss), looks good and minimalistic. I might make one later, or modify this one. But first, I gotta write.

I'm going to be moving the content slowly from the previous blog into kind of "refreshed blog posts", hopefully this helps me to build the habbit of writing more.

Looking forward to seeing how this works out.
