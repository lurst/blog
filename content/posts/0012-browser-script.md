Title: Browser, a script to view html from STDOUT
Date: 2020-10-04
tags: linux, tools
Summary: A script to open a browser with contents from STDOUT to use from the terminal

I've been using this script for 5 years (posted originally on Feb 1, 2015) and it's been very useful to me, so this is me honouring this script for it's service.

It was Inspired and based off of [this gist](https://gist.github.com/defunkt/318247), since that script didn't work with my environment, I decided to improve on it.

### Requirements

You need to have `bash` installed, but this was also tested in `zsh` so feel free to change the hashbang, apart from that, the only thing you need to do is to set the `$BROWSER` environment variable.

```bash
$ export BROWSER="firefox" # or "chromium-browser" or "chrome" or any browser you want
```

And here is the code:

```bash 
#!/bin/bash

if [ -t 0 ]; then
    if [ -n "$1" ]; then
        $BROWSER $1
    else
        cat <<usage
Usage: browser
       pipe html to a browser

$ echo '<h1>hi mom!</h1>' | browser
$ ron -5 man/rip.5.ron | browser
usage
fi
else
    file_path="/dev/shm/browser.${RANDOM}.html"
    cat /dev/stdin > $file_path
    $BROWSER $file_path
fi
```

Just in case I change it, this the version I'm currently using: [https://github.com/LuRsT/Setup/blob/master/bin/browser](https://github.com/LuRsT/Setup/blob/master/bin/browser)
