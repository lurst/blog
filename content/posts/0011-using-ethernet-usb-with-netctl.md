Title: USB Ethernet dongles on netctl
Date: 2020-07-08
tags: linux
Modified: 2020-10-20
Summary: How to configure a usb ethernet dongle for netctl

I was just changing the configuration of my usb hub nest to something more hidden away and my old netctl profile for the USB ethernet dongle ceased to work.

The problem was the interface changed, and I imagine this could happen every time you change the port where the USB dongle is attached to, I still haven't figured out what's the mechanism behind this, but this is important to know because it's what has caused the problem in the first place.

My ethernet netctl profile is this:

```
$ cat /etc/netctl/ethernet
Description='A basic dhcp ethernet connection'
Interface=enp0s20f0u2u4
Connection=ethernet
IP=dhcp
```

Notice the Interface value, it's the "interface" of our network profile. How do you know the name?

```
$ ls /sys/class/net/
```

And I don't know how to identify the right one, but I imagine on Arch linux they all start with `enp`.

- TODO: Figure out how to identify the right interface, answer may be here: https://wiki.archlinux.org/index.php/Network%20configuration
- TODO: Figure out why do the interfaces change their names

The [nectl docs](https://wiki.archlinux.org/index.php/Netctl)
