Title: Scalability? You ain't gonna need it
Date: 2020-07-04
tags: startups
Modified: 2020-07-07
Summary: Why scalability should not be a concern when starting a project

An early stage startup, or any new project, should be built in an exploratory way, as an MVP, in order to get feedback from users so that they know they are building the right thing, the right way, before worrying about scalability.

This is because it's much more important for a startup to understand what problem the product is solving and if it is indeed solving it (therefore getting more and more users) before starting to make it scalable. A product that solves a problem nobody has, needs to scale for no one, because nobody will use it.

In the real world, this plays out by building an MVP, and checking in with users, shortening the feedback loop as much as possible, like Paul Graham suggests, by talking with the users directly, in order to know what the product needs to be before thinking about scalability. Scalability will be a nice problem to have, because it means that it solves a problem for many people.

A product is also never "complete", thinking about scaling can easily end up in an endless loop since we will never know what it will look like in 5 years time, we only know what users need right now.

All of this can be summarised by [YAGNI](https://www.martinfowler.com/bliki/Yagni.html).
