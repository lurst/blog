Title: Books I recommend
Date: 2020-02-12
slug: books

Here are some books I have read and have recommended others to read.

Links go to [Goodreads](https://goodreads.com).

This page will be updated with more descriptions for each book, why I like them and with more books as I read more.

## Programming

- [Code Complete](https://www.goodreads.com/book/show/4845.Code_Complete)
- [The Pragmatic Programmer](https://www.goodreads.com/book/show/4099.The_Pragmatic_Programmer)
- [Version Control With Git](https://www.goodreads.com/book/show/6548113-version-control-with-git)

## Leadership

- [The Manager's Path](https://www.goodreads.com/book/show/33369254-the-manager-s-path)

## Tech related, but novels

- [The Soul Of A New Machine](https://www.goodreads.com/book/show/7090.The_Soul_of_a_New_Machine)
- [The Phoenix Project](https://www.goodreads.com/book/show/17255186-the-phoenix-project)
- [The Unicorn Project](https://www.goodreads.com/book/show/44333183-the-unicorn-project)
- [Masters of Doom](https://www.goodreads.com/book/show/222146.Masters_of_Doom)
