Title: Now
Date: 2020-07-03
Modified: 2024-04-14

- Still trying to get back to blogging
- Still enjoying my current role back in management
- Setting up a shed in the garden and pondering on future gardening projects
- Pondering on replacing my VPC with a home server
- Making my own emacs config almost from scratch
