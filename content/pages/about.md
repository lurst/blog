Title: About
Date: 2022-04-25
Modified: 2022-11-12

I'm Gil and I write code. I'm interested in code, mostly web and TUI tools.

## Links

- My [weekly newsletter](https://buttondown.email/gil.codes) (on pause) where I share content I learned during the week and talk about what happened during it.
- My [youtube channel](https://www.youtube.com/channel/UCb4fl94o3Ma-fFNj6QyhJEg) (on pause) where I share some videos of me coding some things, didatic and fun, if you're into people writing code on neovim.
- My [patreon](https://www.patreon.com/gil_codes) (on pause) account which I'm not using for anything really, it's a placeholder for potential future me.
- My [github](https://github.com/lurst) account where I share most of the code I'm writing.
- My [gitlab](https://github.com/lurst) account where I share less of the code I'm writing. (and this blog)
- My [twitter](https://twitter.com/lurst) account that I don't really use much for writing, I mostly lurk.


## Who I follow

- [Jessie Frazelle](https://blog.jessfraz.com/)
- [Denise Yu](https://deniseyu.io)
- [Jonathan Borichevskiy](https://jborichevskiy.com/)
- [Gee Paw](https://www.geepawhill.org/)
- [Yuan Liu](https://lyncredible.com)
- [Lara Hogan](https://larahogan.me/)
- [Derek Sivers](https://sive.rs/)
- [Dhanish Gajjar](https://dhanishgajjar.com/)
- [Julia Evans](https://jvns.ca/)

