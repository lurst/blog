Title: Plan
Date: 2020-08-01
Modified: 2021-01-18

[_Everybody has a plan until they get punched in the mouth_](https://anopenplan.com/about/)

## November 2022

### Update my projects

I've been neglecting my github projects and other things lately, given I'm now unemployed, I'll take a look at them, archive some,
update others, maybe create new ones.

### Looking for a job

I've been made redundant last week, so I'm looking for a new role currently and trying to enjoy the free time I have.

### Adapting to Cambridge

Moved to Cambridge last week, it's a lovely city and I want to get to know it better, my neighbours and places, like the Makespace.

## July 2022

### Writing more

I have started to journal more and get back into that habit, I want to move that new habit into
writing more on this blog and other places, my role has a bit of writing involved which does
help too.

### Building a great second brain

I've been using obsidian (and org-roam earlier) to build a good knowledge database for all the
things I care about learning, which should help with the goal above, the idea is to have a
place where I can both recall things I learned but also a place to find content for future posts.

### My setup

I've been doing small improvements to my own setup, like using dmenu instead of rofi (smaller
footprint), and to improve the SEO of this blog (as much as possible given it's a gitlab pages
site).

## January 2021

### Thriving in my new role

Being [so good they can't ignore me](https://www.goodreads.com/book/show/13525945-so-good-they-can-t-ignore-you) at my new role, it's been almost 3 months, now it's my time to shine.

### Making my own emacs config

I've been using doom emacs for a while and I'm very adapted, but it's much nicer if I actually understand what's happening, and I should really learn how emacs and elisp work and the way I'm doing that is by replicating my doom config, or something similar at least.

### Reading one book a month

Maybe I'm being conservative with this goal, but I've been slacking off on reading, I'm now making it a habit to read a bit every single day, but I think that a goal like this would help push me to read more than just a little. If I end up reading more than one book, then I'll move it to 2 in the next months.

## September 2020

### Moving jobs

I quit my job as Backend lead and will be going back into engineering as a Backend engineer, so I'm winding down my work at my current company.

### Writing

I haven't really written much lately, this transition period before/after quitting has been stressful for me. I'm now feeling a bit more relaxed because everything is set in motion. I have also spent some time improving my knowledge management system, so I can hopefully dig up some gems I have and turn them into blog posts.

### Deep focus

I've been experimenting with some pomodoros and blocking out distractions (like twitter/HN via /etc/hosts file editing) so I can have some periods of deep focus while coding at work. One issue of being a manager is that I try to keep myself interruptable so I can help out. This isn't great for work such as coding or writing, so I'm trying to get into a routine of forcing some deep focus time so I can increase my ability to focus.

## August 2020

### Writing

My plan is to keep my habit of writing weekly, I'm focusing on the system now rather than a goal like writing a book (which is something I'd like to do) because it's how these things work best for me.

### Coding

Learning more about [CQRS](https://www.martinfowler.com/bliki/CQRS.html), I used to work in a company that used this pattern, I'm now using Django, so I miss it, and I'm rusty with it. I got inspired by Europython last week and I want to re-learn it and get better at it.

### Minimalism

Embryonic plan, still mostly in my head, but I'm doing an experiment of unplugging my gaming computer so I don't play games, and it's been working fine, I miss the games, but I have so many other things to do that it's not painful anymore. Focusing on reducing my identity (e.g. I'm a gamer) seems to work better than simply decluttering.
