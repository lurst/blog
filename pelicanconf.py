#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = "Gil"
SITENAME = "Gil's blog about code, technology, and leadership"
SITEHEADERNAME = "Gil's blog"  # This is only for the site header
SITEURL = ""

PATH = "content"
OUTPUT_PATH = "public"

TIMEZONE = "Europe/London"

DEFAULT_LANG = "en"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = "feeds/all.atom.xml"
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = ()

# Social widget
SOCIAL = (
    ("github", "https://github.com/lurst"),
    ("gitlab", "https://gitlab.com/lurst"),
    ("twitter", "https://twitter.com/lurst"),
    ("linkedin", "https://bit.ly/gils_linkedin"),
    ("youtube", "https://www.youtube.com/channel/UCb4fl94o3Ma-fFNj6QyhJEg"),
)

DEFAULT_PAGINATION = 5

THEME = "template"

STATIC_PATHS = ["images", "extra"]

CUSTOM_CSS_URL = "/static/custom.css"

# set url of custom.css to EXTRA_PATH_METADATA.
EXTRA_PATH_METADATA = {
    "extra/custom.css": {"path": "static/custom.css"},
}

TYPOGRIFY = True

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

PAGE_URL = "pages/{slug}"
